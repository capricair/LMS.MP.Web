function ImageView(options) {

    //使用严格模式
    "use strict"

    //初始化设置
    var imageview = this;
	var defaults = {
		maxScale: 20,
		minScale: 0.5,
		container: null,
		selector: null,
		current: 0,
	}
	var settings = $.extend(defaults, options);

	var slide = (function () {
	    //公用touch对象
	    var mySwiper = null;
	    var noswipingClass = "swiper-no-swiping";
	    var touch = _getDefaultTouch();
	    var scroll = {};
	    var viewsize = {
	        width: function () { return document.documentElement.clientWidth; },
	        height: function () { return document.documentElement.clientHeight; },
	        digonal: function () {
	            return _diagonal(0, 0, document.documentElement.clientWidth, document.documentElement.clientHeight);
	        }
	    };
	    var IsAndroid = /android/i.test(navigator.userAgent);
	    var IsWeChat = /micromessenger/i.test(navigator.userAgent);

		//添加slide的HTML
	    function _appendHTML() {
			var images = $(settings.container).find(settings.selector);
			var imagesHTML = "";
			for (var i = 0; i < images.length; i++) {
			    imagesHTML += '<div class="swiper-slide" data-url="' + images.eq(i).attr("data-url") + '">' +
                                  '<img class="swiper-slide-image" />' +
			                  '</div>';
			}
			var $container = $('<div id="swiper-imageview" class="swiper-container swiper-imageview">' +
					               '<div class="swiper-wrapper">' +
						               imagesHTML +
					               '</div>' +
					               '<div class="swiper-pagination"></div>' +
				               '</div>');
			if ($("#swiper-imageview").length == 0) {
			    $(document.body).append($container);
			}
			setTimeout(function () { $container.css("visibility", "visible").addClass("open"); });
		}

	    //初始化滑动插件对象
		function _init() {
		    _appendHTML();
		    //monitor.init();
		    recordWindowScrollY();
		    _disabledScroll();
			mySwiper = new Swiper("#swiper-imageview", {
				initialSlide: settings.current,
				pagination: "#swiper-imageview .swiper-pagination",
				spaceBetween: 1,  //每一页之间的间隔
				noSwiping: true,
				onInit: function (swp) {
				    _initHandler(swp);
				},
                ////插件touch事件在禁用滑动后会不触发
				//onTouchStart: function (swp, evt) {
				//    _touchStartHandler(swp, evt);
				//},
				//onTouchMove: function (swp, evt) {
				//    _touchMoveHandler(swp, evt);
				//},
				//onTouchEnd: function (swp, evt) {
				//    _touchEndHandler(swp, evt);
				//},
				onSlideChangeEnd: function (swp) {
				    _slideChangeEndHandler(swp);
				}
			});
			return imageview;
		}

		function _initHandler(swp) {

		    var $container = $("#swiper-imageview");

		    $container.find(".swiper-slide img").each(function () {
		        var image = this;
		        var slide = $(image).closest(".swiper-slide");
		        image.onload = function () {
		            $(image).data("size", image.width + "," + image.height);
		            if (image.height > viewsize.height()) {
		                $(image).css({
		                    top: "auto",
		                    bottom: "auto"
		                });
		            }
		        }
		        image.src = slide.attr("data-url");
		    });

		    $container.on("touchstart mousedown", function (e) {
		        _touchStartHandler(swp, e.originalEvent || e);
		    });
		    $container.on("touchmove mousemove", function (e) {
		        _touchMoveHandler(swp, e.originalEvent || e);
		    });
		    $container.on("touchend mouseup", function (e) {
		        _touchEndHandler(swp, e.originalEvent || e);
		    });
		}

		function _touchStartHandler(swp, evt) {
		    if (evt.touches) {
		        mySwiper.currentSlide = getCurrentSlide();
		        mySwiper.currentImage = getCurrentImage();
		        mySwiper.currentImage.size = getImageOriginSize(mySwiper.currentImage);
		        //记录初始坐标，用于计算move偏移量
		        //初始化结束坐标，touchend事件中touches记录值已全部清除，如果只点击没移动，不会触发touchmove事件，所以要在此初始化end坐标
		        touch.end.x = touch.start.x = evt.touches[0].clientX;
		        touch.end.y = touch.start.y = evt.touches[0].clientY;
		        touch.end.timestamp = touch.start.timestamp = new Date().getTime();
                //判断是否为多指操作
		        if (evt.touches.length > 1) {
		            //设置缩放操作标识
		            touch.IsScale = true;
                    //双指操作记录第二个坐标
		            touch.start.x1 = evt.touches[1].clientX;
		            touch.start.y1 = evt.touches[1].clientY;
		            touch.end.x1 = evt.touches[1].clientX;
		            touch.end.y1 = evt.touches[1].clientY;
		            touch.startDiagonal = _diagonal(touch.start.x, touch.start.y, touch.start.x1, touch.start.y1);
		            //阻止滑动，防止缩放时触发滑动操作
		            disabledSwiping();
		            //监听横向滚动事件
		            bindSlideScrollX(mySwiper.currentSlide);
                    //取消图片最大宽度限制
		            if (mySwiper.currentImage.css("max-width") != "none") {
		                mySwiper.currentImage.css("max-width", "none");
		            }
		        }
		    } else {
		        touch.mouse = { start: { x: evt.clientX, y: evt.clientY, timestamp: new Date().getTime() } };
		    }
		}

		function _touchMoveHandler(swp, evt) {

		    if (!evt.touches) return;

		    var x = evt.touches[0].clientX, y = evt.touches[0].clientY, x1, y1;

		    if (evt.touches.length > 1) {
		        //设置缩放操作标识
		        touch.IsScale = true;
                //记录第二个触摸点坐标
                x1 = evt.touches[1].clientX, y1 = evt.touches[1].clientY;
		        //设置图片缩放变量
                var currentD = _diagonal(x, y, x1, y1);
                var increment = (currentD - touch.startDiagonal) / viewsize.digonal();
                var scale = touch.start.scale = touch.end.scale + increment;
                var width = mySwiper.currentImage.size.width * scale;
                mySwiper.currentImage.width(width);
		    }
		    $.extend(touch.end, { x: x, y: y, x1: x1, y1: y1 });
		}

		function _touchEndHandler(swp, evt) {

		    var closeTimestamp = 160;
		    touch.end.scale = touch.start.scale;

		    if (evt.changedTouches) {
		        var $image = getCurrentImage();
		        var size = getImageOriginSize($image);
		        //记录结束坐标和时间戳
		        touch.end.x = evt.changedTouches[0].clientX;
		        touch.end.y = evt.changedTouches[0].clientY;
		        if (evt.changedTouches.length > 1) {
		            touch.end.x1 = evt.changedTouches[1].clientX;
		            touch.end.y1 = evt.changedTouches[1].clientY;
		        }
		        touch.end.timestamp = new Date().getTime();
                //如果图片被缩放小于原始大小，则恢复到原始大小
		        if ($image.width() < size.width) {
		            touch.start.scale = touch.end.scale = 1;
		            resizeImage($image);
		        }
		        //如果手指没移动，并且按住屏幕不超过指定值，则关闭轮播，click事件不会判断手指移动，为了防止与移动和缩放冲突
		        if (touch.end.x - touch.start.x == 0 &&
                    touch.end.y - touch.start.y == 0 &&
                    touch.end.timestamp - touch.start.timestamp < closeTimestamp) {

		            _destroySwiper(swp);
		        }
		        //取消缩放操作标识，双指一般不是同时离开屏幕，所以要延迟取消
		        setTimeout(function () { touch.IsScale = false }, 500);
		    } else if (touch.mouse) {
		        touch.mouse.end = { x: evt.clientX, y: evt.clientY, timestamp: new Date().getTime() };
		        if (touch.mouse.end.x - touch.mouse.start.x == 0 &&
                    touch.mouse.end.y - touch.mouse.start.y == 0 &&
                    touch.mouse.end.timestamp - touch.mouse.start.timestamp < closeTimestamp) {

		            _destroySwiper(swp);
		        }
		    }
		}

		function _slideChangeEndHandler(swp) {
		    mySwiper = mySwiper || swp;
		    var $this = mySwiper.currentSlide = getCurrentSlide();
		    var $image = mySwiper.currentImage = getCurrentImage();
		    //滑动到下一个图片前把所有图片回归初始状态，否则滑动的位置会出错
		    _resetSlides(swp, $image);
		}

		function getIncrement(x, x1, y, y1) {
		    var xi = Math.abs(x - x1) - Math.abs(touch.end.x - touch.end.x1);
		    var yi = Math.abs(y - y1) - Math.abs(touch.end.y - touch.end.y1);
		    return Math.max(xi, yi);
		}

		function getCurrentSlide() {
		    return $(mySwiper.slides[mySwiper.activeIndex]);
		}

		function getCurrentImage() {
		    return getCurrentSlide(mySwiper).find(".swiper-slide-image");
		}

		function getImageOriginSize(image) {
		    var arr = $(image).data("size").split(",");
		    return { width: parseFloat(arr[0]), height: parseFloat(arr[1]) };
		}

		function enableSwiping() {
		    getCurrentSlide().removeClass(noswipingClass);
		}

		function disabledSwiping() {
		    getCurrentSlide().addClass(noswipingClass);
		}

		function resizeImage(image) {
		    var size = getImageOriginSize(image);
		    mySwiper.currentSlide.off("scroll")
                .removeClass(noswipingClass);
		    $(image).css({
		        "-webkit-transition": "all 0.3s ease",
		        "transition": "all 0.3s ease"
		    }).one("webkitTransitionEnd transitionend", function () {
		        $(image).css({
		            "width": "auto",
		            "max-width": "100%",
		            "-webkit-transition": "none",
		            "transition": "none"
		        });
		    }).css("width", size.width);
		}

		function bindSlideScrollX(slide) {
		    var limit = 30;
		    function swipeShortLock() {
		        mySwiper.lockSwipes();
		        setTimeout(function () { mySwiper.unlockSwipes(); }, 500);
		    }
		    $(slide).on("scroll", function () {
		        var $this = $(this);
		        var scrollLeft = $this[0].scrollLeft;
		        var scrollEnd = $(this)[0].scrollWidth - $this.width();
		        if (scrollLeft < limit && !touch.IsScale) {
		            mySwiper.slidePrev();
		            swipeShortLock();
		        } else if (scrollLeft > scrollEnd + limit && !touch.IsScale) {
		            mySwiper.slideNext();
		            swipeShortLock();
		        }
		        $this.data("scroll", "imageview");
		    });
		}

		function unbindAllSlideScrollX(slide) {
		    var $slides = $("#swiper-imageview [data-scroll='imageview']");
		    $slides.removeAttr("data-scroll").off("scroll");
		}

        //获取坐标数值
		function _getPosNumber(el, direction) {
		    try {
		        return parseFloat($(el).css(direction).replace("px", ""));
		    } catch (e) {
		        console.log(e.message);
		    }
		}

        //销毁轮播图控件
		function _destroySwiper(swp) {
		    _enableScroll();
		    swp.destroy(false);
		    $(swp.container).one("webkitTransitionEnd transitionend", function () {
		        $(this).remove();
		    }).removeClass("open");
		    //$("#swiperScale").remove();
		    swp = null;
		    setWindowScrollY();
		    monitor.remove();
		}

        //禁用页面滚动
		function _disabledScroll() {
		    $("html,body").css({
		        //height: "100%",
		        overflow: "visible"
		    });
		}

        //启用页面滚动
		function _enableScroll() {
		    $("html,body").css({
		        //height: "",
		        overflow: ""
		    });
		}

        //重置所有slide
		function _resetSlides(swiper, image) {
		    touch = _getDefaultTouch();
		    unbindAllSlideScrollX();
            var each = swiper.slides.forEach ? "forEach" : "each";
            swiper.slides[each](function (val, idx, obj) {
		        var $slide = $(this);
		        var $image = $slide.find(".swiper-slide-image");
		        $slide.removeClass(noswipingClass)[0].scrollTop = 0;
		        $image.css({
		            "width": "auto",
		            "max-width": "100%"
		        });
		    });
		}

		//求两点之间的对角线长度
		function _diagonal(x, y, x1, y1) {
		    var width = Math.abs(x - x1);
			var height = Math.abs(y - y1);
			return Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2)).toFixed(2);
		}

		//设置多前缀transform
		function _setTransform(el, val) {
			["-webkit-transform", "-ms-transform", "transform"].forEach(function (attr, index, item) {
				$(el).css(attr, val);
			})
		}

		function setSlideTransform(el) {
		    $(el).css({
		        "-webkit-transform": "translate3d(" + touch.pos.x + "px, " + touch.pos.y + "px, 0) scale3d(" + touch.scale + ", " + touch.scale + ", 1)",
		        "transform": "translate3d(" + touch.pos.x + "px, " + touch.pos.y + "px, 0) scale3d(" + touch.scale + ", " + touch.scale + ", 1)"
		    });
		}

		function _getDefaultTouch() {
		    return {
		        startDiagonal: 0,
		        imageSize: { width: 0, height: 0 },
		        start: { x: 0, y: 0, x1: 0, y1: 0, scale: 1, timestamp: new Date().getTime() },
		        end: { x: 0, y: 0, x1: 0, y1: 0, scale: 1, timestamp: new Date().getTime() },
		        IsScale: false
		    };
		}

		function recordWindowScrollY() {
		    scroll.scrollYCacheKey = window.location.href + "SwiperImageViewScroll";
		    if (sessionStorage) {
		        var startScrollY = window.scrollY ? window.scrollY : document.body.scrollTop;
		        sessionStorage.setItem(scroll.scrollYCacheKey, startScrollY);
		    }
		}

		function setWindowScrollY() {
		    var startScrollY = sessionStorage.getItem(scroll.scrollYCacheKey);
		    if (sessionStorage && startScrollY) {
		        startScrollY = parseInt(startScrollY);
		        window.scrollTo(0, startScrollY);
		    }
		}

		return {
			init: _init
		}
	})();

    //变量监视器
	var monitor = (function () {
		var _init = function () {
			if ($(".swiper-monitor").length == 0) {
				$("body").append("<div class='swiper-monitor'></div>");
			}
		}
		var _update = function (data) {
			var monitorHTML = "";
			for (var key in data) {
				monitorHTML += "<div>" + key + ": " + data[key] + "</div>";
			}
			$(".swiper-monitor").html(monitorHTML);
		}
		var _remove = function () {
		    $(".swiper-monitor").remove();
		}
		return {
			init: _init,
			update: _update,
            remove: _remove
		}
	})();

	return slide.init();
}