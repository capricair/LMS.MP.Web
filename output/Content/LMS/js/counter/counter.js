function Counter(container, count, options) {

    var defaults = {
        init: true,
        count: count,
        complete: null,
        timespan: 1000,
        decrement: 1
    },
    settings = $.extend(defaults, options);

    var $container = $(container),
        clearId,
        status = 0;

    function init() {
        count = defaults.count;
        print();
    }

    function print() {
        $container.text(count);
    }

    function decrement() {
        count = (count - settings.decrement).toFixed(2);
    }

    function start() {
        if (status == 1 || count <= 0) return;
        status = 1;
        clearTimeout(clearId);
        clearId = setInterval(function () {
            decrement();
            $container.text(count);
            if (count <= 0) {
                clearInterval(clearId);
                settings.complete && settings.complete();
            }
        }, settings.timespan);
    }

    function pause() {
        status = 0;
        clearInterval(clearId);
    }

    function reset() {
        count = defaults.count;
        pause();
        init();
    }

    settings.init && init();

    return {
        start: start,
        pause: pause,
        reset: reset
    }
}