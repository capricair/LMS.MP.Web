(function () {
    var key = location.host + "_bigger_theme_src";
    if (localStorage){
        var themesrc = localStorage.getItem(key);
        if (themesrc){
            document.querySelector("link[href*='theme']").href = themesrc;
        }
    }
    document.addEventListener("click", function (e) {
        var attrName = "data-theme-src";
        var target = null;
        for (var i=0; i<e.path.length; i++){
            var _this = e.path[i];
            if (_this.tagName.toLowerCase() == "body") break;
            if (_this.hasAttribute(attrName)){
                target = _this;
                break;
            }
        }
        if (target){
            var themesrc = target.getAttribute(attrName);
            document.querySelector("link[href*='theme']").href = themesrc;
            localStorage && localStorage.setItem(key, themesrc);
        }
    }, false);
})();