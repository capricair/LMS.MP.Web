/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-car-light': '&#xe934;',
		'icon-module': '&#xe930;',
		'icon-file-list': '&#xe931;',
		'icon-log': '&#xe932;',
		'icon-vip': '&#xe933;',
		'icon-search-text': '&#xe92f;',
		'icon-notice': '&#xe92e;',
		'icon-ok': '&#xe900;',
		'icon-right': '&#xe901;',
		'icon-user': '&#xe902;',
		'icon-home': '&#xe903;',
		'icon-book': '&#xe904;',
		'icon-view-light': '&#xe905;',
		'icon-sandglass': '&#xe906;',
		'icon-half-right': '&#xe907;',
		'icon-edit': '&#xe908;',
		'icon-pencil': '&#xe909;',
		'icon-paper': '&#xe90a;',
		'icon-lock': '&#xe90b;',
		'icon-lock-fill': '&#xe90c;',
		'icon-more-vertical': '&#xe90d;',
		'icon-more': '&#xe90e;',
		'icon-laugh': '&#xe90f;',
		'icon-keyboard': '&#xe910;',
		'icon-compass': '&#xe911;',
		'icon-compass-fill': '&#xe912;',
		'icon-search': '&#xe913;',
		'icon-plus': '&#xe914;',
		'icon-minus': '&#xe915;',
		'icon-fire': '&#xe916;',
		'icon-local': '&#xe917;',
		'icon-local-fill': '&#xe918;',
		'icon-setting': '&#xe919;',
		'icon-setting-fill': '&#xe91a;',
		'icon-plus-circle': '&#xe91b;',
		'icon-plus-circle-fill': '&#xe91c;',
		'icon-minus-circle-fill': '&#xe91d;',
		'icon-minus-circle': '&#xe91e;',
		'icon-heart-fill': '&#xe91f;',
		'icon-heart': '&#xe920;',
		'icon-menu': '&#xe921;',
		'icon-time': '&#xe922;',
		'icon-date': '&#xe923;',
		'icon-comment': '&#xe924;',
		'icon-view': '&#xe925;',
		'icon-praise': '&#xe926;',
		'icon-praised': '&#xe927;',
		'icon-circle-arrow': '&#xe928;',
		'icon-smile': '&#xe929;',
		'icon-star': '&#xe92a;',
		'icon-star-fill': '&#xe92b;',
		'icon-arrow': '&#xe92c;',
		'icon-loading': '&#xe92d;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
