window.ReactWebComponent = $.extend(window.ReactWebComponent, (function () {

    var List = React.createClass({
        loadData: function () {
            var _this = this;
            if (_this.props.source){
                _this.setState({ data: _this.props.source });
                return;
            }
            $.ajax({
                url: _this.props.url,
                type: "GET",
                data: _this.props.data,
                dataType: "json",
                cache: false,
                success: function (data) {
                    if (/success/i.test(data.ResultCode)){
                        _this.setState({data: data.Data});
                    }
                },
                error: function (xhr, status, err) {
                    console.log(_this.props.url, status, err.toString());
                }
            });
        },
        componentDidMount: function () {
            this.loadData();
        },
        getInitialState: function () {
            return {data: []};
        },
        render: function () {
            return (
                <ListItem data={ this.state.data } />
            );
        }
    });

    var ListItem = React.createClass({
        render: function () {
            var data = this.props.data;
            if (!data || data.length == 0) return null;
            var items = data.map(function (item) {
                var itemClass = [];
                if (item.Status) itemClass.push(item.Status);
                if (item.Corner) itemClass.push("corner-box");
                return (
                    <li className={ itemClass.join(" ") } key={ item.ID }>
                        <ListItemThumb url={ item.Thumb } />
                        <ListItemInfo title={ item.Title } secondaries={ item.Secondaries } />
                        { item.Corner && <ListItemCorner corner={ item.Corner } /> }
                    </li>
                )
            });
            return (
                <ul className="list list-thumb list-corner">
                    { items }
                </ul>
            );
        }
    });

    var ListItemThumb = React.createClass({
        render: function () {
            return (
                <div className="align-left">
                    { this.props.url && <div className="cover" style={{ backgroundImage: "url(" + this.props.url + ")" }}></div> }
                </div>
            )
        }
    });

    var ListItemInfo = React.createClass({
        render: function () {
            var secondaries = this.props.secondaries.map(function (secondary, index) {
                return (
                    <p className="secondary" key={index}>{secondary}</p>
                )
            });
            return (
                <div className="align-right">
                    <div className="info">
                        <p className="title">{ this.props.title }</p>
                        { secondaries }
                    </div>
                </div>
            )
        }
    });

    var ListItemCorner = React.createClass({
        render: function () {
            var corner = null;
            if (this.props.corner.icon){
                corner = (
                    <div className="corner-wrapper">
                        <i className={ "icomoon " + (this.props.corner.icon || "") }></i>
                    </div>
                )
            }else if (this.props.corner.text){
                corner = (
                    <div className="corner-wrapper corner-text">
                        { this.props.corner.text }
                    </div>
                )
            }
            return (
                <div className={ "corner " + (this.props.corner.bg || "") }>
                    { corner }
                </div>
            )
        }
    });

    return {
        List: List
    }

})());