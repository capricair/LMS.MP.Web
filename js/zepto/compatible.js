// `$.zepto.Z` swaps out the prototype of the given `dom` array
// of nodes with `$.fn` and thus supplying all the Zepto functions
// to the array. Note that `__proto__` is not supported on Internet
// Explorer. This method can be overriden in plugins.
;(function($){

    var _zepto = $.zepto;

    _zepto.Z = function (dom, selector) {

        dom = dom || []

        // 支持ie10,主要是支持wp8
        if(/msie/i.test(navigator.userAgent)){
            for(var func in $.fn){
                dom[func] = $.fn[func];
            }
        }
        else{
            dom.__proto__ = $.fn
        }

        dom.selector = selector || ''

        return dom
    }

    _zepto.Z.prototype = $.fn

})(Zepto);

